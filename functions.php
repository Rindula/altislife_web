<?php

function translateCrime($crime) {
    $ret = "";
    switch ($crime) {
        case '187V':
            $ret = "Fahrlässige Tötung";
            break;
        
        case '901':
            $ret = "Ausbruch";
            break;
        
        case '215':
            $ret = "Versuchter Autodiebstahl";
            break;
        
        case '211A':
            $ret = "Schwerer Raub";
            break;
        
        case '207':
            $ret = "Entführung";
            break;
        
        case '207A':
            $ret = "Versuchte Entführung";
            break;
        
        case '488':
            $ret = "Diebstahl";
            break;
        
        case '480':
            $ret = "Fahrerflucht";
            break;
        
        case '459':
            $ret = "Einbruch";
            break;
        
        case '666':
            $ret = "Steuerhinterziehung";
            break;
        
        case '667':
            $ret = "Terrorismus";
            break;
        
        case '1':
            $ret = "Fahren ohne Führerschein";
            break;
        
        case '2':
            $ret = "Fahren auf der falschen Straßenseite";
            break;
        
        case '3':
            $ret = "Nicht Beachtung der Signalisierungen";
            break;
        
        case '4':
            $ret = "Geschwindigkeitsüberschreitung";
            break;
        
        case '5':
            $ret = "Fahren ohne Licht";
            break;
        
        case '6':
            $ret = "Fahren ohne Helm";
            break;
        
        case '7':
            $ret = "Parksünder";
            break;
        
        case '10':
            $ret = "Diebstahl Staatlichen Eigentums";
            break;
        
        case '11':
            $ret = "Führen eines illegalen KFZ";
            break;
        
        case '12':
            $ret = "Fliegen über die Stadt ohne Genehmigung";
            break;
        
        case '13':
            $ret = "Straßensperrung ohne Genehmigung";
            break;
        
        case '14':
            $ret = "Führen / Besitz eines illegalen Gegenstandes";
            break;
        
        case '18':
            $ret = "Behinderung der Staatsgewalt";
            break;
        
        case '19':
            $ret = "Flucht";
            break;
        
        case '20':
            $ret = "Beleidigung";
            break;
        
        case '21':
            $ret = "Beamtenbeleidigung";
            break;
        
        case '22':
            $ret = "Verstoß gegen Betäubungsmittelgesetz";
            break;
        
        case '24':
            $ret = "Das Töten eines Zivilisten";
            break;
        
        case '25':
            $ret = "Das Töten eines Beamten";
            break;
        
        case '26':
            $ret = "Illegale Migration";
            break;
        
        default:
            $ret = "ERROR: Nicht definierte Variable";
            break;
    }
    return $ret;
}