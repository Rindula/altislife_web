<?php
    if (!isset($_GET["name"])) {
        http_response_code(400);
        die();
    }
?>
<!doctype html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

        <title>Spielerauswahl</title>
    </head>
    <body class="container bg-light">
        <div class="jumbotron">
            <h1>Spielerabfrage &lt;<?= $_GET["name"] ?>&gt;</h1> 
            <p>Hier kannst du deine aktuellen Spielerinformationen einsehen.</p> 
        </div>
        <table class="table table-bordered text-center">
            <tr>
                <td><a class="d-block" href="actions/wantedlist.php?name=<?= $_GET["name"] ?>">Fahndungsliste abfragen</a></td>
                <td><a class="d-block" href="actions/playerinfo.php?name=<?= $_GET["name"] ?>">Spielerinformation einsehen</a></td>
            </tr>
            <tr>
                <td><a class="d-block" href="actions/garage.php?name=<?= $_GET["name"] ?>">Spielergarage einsehen</a></td>
                <td><a class="d-block" href="actions/messages.php?name=<?= $_GET["name"] ?>">Nachrichtenverlauf einsehen</a></td>
            </tr>
        </table>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
</html>