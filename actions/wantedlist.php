<?php
    if (!isset($_GET["name"])) {
        http_response_code(400);
        die();
    }

    if (isset($_GET["p"])) {
        include_once "../readconfig.php";
        include_once "../functions.php";

        list($user, $pass) = array($db["user"], $db["password"]);
        $dbh = new PDO('mysql:host='.$db["host"].';dbname='.$db["database"], $user, $pass);
        $dbh->query('SET NAMES utf8');

        $sth = $dbh->prepare('SELECT * FROM wanted WHERE wantedName = :wname AND active = 1 ORDER BY insert_time');
        $sth->bindParam(':wname', $_GET["name"]);
        if ($sth->execute()) {
            while ($row = $sth->fetch()) {
                $str = str_replace("[", "", $row["wantedCrimes"]);
                $str = str_replace("]", "", $str);
                $str = str_replace("`", "", $str);
                $str = str_replace("\"", "", $str);
                if ($str !== "") {
                    $str = explode(",",$str);
                } else {
                    continue;
                }

                foreach ($str as $value) {
                    $translated = translateCrime($value);
                    echo "<tr><td>$translated</td></tr>";
                }
                echo "<tr><td class='display-4'>$".$row["wantedBounty"]."</td></tr>";
            }
        }
        
        die();

    }
?>
<!--
    ('76561198057155308', 'mohammad al hasab', '\"[`211A`,`211A`,`187`,`211A`,`187`,`211A`,`211A`]\"', 84000, 1, '2018-06-15 21:25:11');
    ('76561198060073729', 'Mr.Pikland', '[]', 0, 0, '2018-05-18 14:43:47');
-->
<!doctype html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

        <script>
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                document.getElementById("table").innerHTML = xhr.responseText;
                if (xhr.status != 200) {
                    document.getElementById("table").innerHTML = "<h1 class='display-4'>Es gab einen Fehler bei der Datenbankabfrage</h1>"
                }
            }
        }
        xhr.open('GET', 'wantedlist.php?name=<?= $_GET["name"] ?>&p', true);
        xhr.send(null);
        </script>

        <title><?= $title ?></title>
    </head>
    <body class="container">
        <a href="javascript:history.back()" class="btn btn-block btn-outline-danger">Zurück</a>
        <h1 class="display-2">Fahndungsliste<br>&lt;<?= $_GET["name"] ?>&gt;</h1>
        <table class="table table-striped" id="table">
            <!-- Tabelle wird via JavaScript gefüllt -->
        </table>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
</html>